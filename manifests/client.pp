# Setup for ossec client
class wazuh_macos::client(
  $ossec_active_response      = true,
  $ossec_rootcheck            = true,
  $ossec_rootcheck_frequency  = 36000,
  $ossec_rootcheck_checkports = true,
  $ossec_rootcheck_checkfiles = true,
  $ossec_server_ip            = undef,
  $ossec_server_hostname      = undef,
  $ossec_server_port          = '1514',
  $ossec_scanpaths            = [],
  $ossec_emailnotification    = 'yes',
  $ossec_ignorepaths          = [],
  $ossec_local_files          = $::wazuh_macos::params::default_local_files,
  $ossec_syscheck_frequency   = 43200,
  $ossec_config_profiles      = ['darwin', 'darwin16', 'darwin16.6'],
  $agent_name                 = $::hostname,
  $agent_ip_address           = $::ipaddress,
  $agent_package_name         = $::wazuh_macos::params::agent_package,
  $agent_package_version      = 'present',
  $agent_service_name         = $::wazuh_macos::params::agent_service,
  $manage_client_keys         = 'export',
  $agent_seed                 = undef,
  $max_clients                = 3000,
  $ar_repeated_offenders      = '',
  $service_has_status         = $::wazuh_macos::params::service_has_status,
  $ossec_conf_template        = 'wazuh_macos/wazuh_agent.conf.erb',
) inherits wazuh_macos::params {
  validate_bool(
    $ossec_active_response, $ossec_rootcheck
  )
  # This allows arrays of integers, sadly
  # (commented due to stdlib version requirement)
  validate_array($ossec_ignorepaths)
  validate_string($agent_package_name)
  validate_string($agent_service_name)

  if ( ( $ossec_server_ip == undef ) and ( $ossec_server_hostname == undef ) ) {
    fail('must pass either $ossec_server_ip or $ossec_server_hostname to Class[\'wazuh_macos::client\'].')
  }

  case $::kernel {
    'Darwin' : {
      file {
        '/var/tmp/wazuh-agent-2.0.1-1.pkg':
          owner  => 'root',
          group  => 'admin',
          mode   => '0774',
          source => 'puppet:///modules/wazuh_macos/wazuh-agent-2.0.1-1.pkg',
      }

      package { $agent_package_name:
        ensure          => $agent_package_version,
        provider        => 'pkgdmg',
        source          => "/var/tmp/wazuh-agent-2.0.1-1.pkg",
        install_options => [ '-target /' ],
        require         => File['/var/tmp/wazuh-agent-2.0.1-1.pkg'],
      }

      file { '/Library/Ossec/lib':
        ensure  => directory,
        owner   => 'root',
        group   => 'ossec',
        mode    => '0550',
        source  => "puppet:///modules/wazuh_macos/lib",
        recurse => true,
        require => Package[$agent_package_name],
      }
    }
    default: { fail('OS not supported') }
  }

  service { $agent_service_name:
    ensure    => running,
    enable    => true,
    hasstatus => $service_has_status,
    hasrestart => false,
    pattern   => 'ossec-execd',
    restart   => '/Library/Ossec/bin/ossec-control restart',
    start     => '/Library/Ossec/bin/ossec-control start',
    require   => Package[$agent_package_name],
  }

  concat { 'ossec.conf':
    path    => $wazuh_macos::params::config_file,
    owner   => $wazuh_macos::params::config_owner,
    group   => $wazuh_macos::params::config_group,
    mode    => $wazuh_macos::params::config_mode,
    require => Package[$agent_package_name],
    notify  => Service[$agent_service_name],
  }

  concat::fragment {
    default:
      target => 'ossec.conf',
      notify => Service[$agent_service_name];
    'ossec.conf_header':
      order   => 00,
      content => "<ossec_config>\n";
    'ossec.conf_agent':
      order   => 10,
      content => template($ossec_conf_template);
    'ossec.conf_footer':
      order   => 99,
      content => '</ossec_config>';
  }

  if ( $manage_client_keys == 'export' ) {
    concat { $wazuh_macos::params::keys_file:
      owner   => $wazuh_macos::params::keys_owner,
      group   => $wazuh_macos::params::keys_group,
      mode    => $wazuh_macos::params::keys_mode,
      notify  => Service[$agent_service_name],
      require => Package[$agent_package_name]
    }
    # A separate module to avoid storeconfigs warnings when not managing keys
    class { 'wazuh_macos::export_agent_key':
      max_clients      => $max_clients,
      agent_name       => $agent_name,
      agent_ip_address => $agent_ip_address,
      agent_seed       => $agent_seed,
    }
  }
}
