# Paramas file
class wazuh_macos::params {
  case $::osfamily {
    'Darwin': {
      $config_file = '/Library/Ossec/etc/ossec.conf'
      $shared_agent_config_file = '/Library/Ossec/etc/shared/agent.conf'
      $config_mode = '0440'
      $config_owner = 'root'
      $config_group = 'ossec'

      $keys_file = '/Library/Ossec/etc/client.keys'
      $keys_mode = '0440'
      $keys_owner = 'root'
      $keys_group = 'ossec'

      $agent_service  = 'com.wazuh.agent' # ['ossec-execd', 'ossec-agentd', 'ossec-logcollector', 'ossec-syscheckd']
      $agent_package  = 'wazuh-agent-2.0-1'
      $server_service = ''
      $server_package = ''
      $service_has_status  = false

      $default_local_files = {
        '/var/log/system.log'                      => 'syslog',
        '/Library/Ossec/logs/active-responses.log' => 'syslog',
      }
    }
    default: {
      fail("Module ${module_name} is not supported on ${::osfamily}")
    }
  }
}
